#!/usr/bin/env python3

"""GPX Course Profile Plotter -- Plotting module
    Python Version  : 3.5.5 in windows
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib.font_manager as fm
import numpy
import io

__author__  = 'Seijung Park'
__license__ = 'GPLv2'
__version__ = '0.2.1'
__date__    = '2018-03-27'


path1 = '/usr/share/fonts/truetype/nanum/NanumMyeongjoBold.ttf'
#path2 = 'c:\\windows\\fonts\\NGULIM.ttf'
#path3 = 'c:\\windows\\fonts\\h2gtrm.ttf'
#path4 = 'c:\\windows\\fonts\\h2gtre.ttf'
#path5 = 'C:\\Windows\\Fonts\\YSOS07.TTF'
#path6 = 'C:\\Windows\\Fonts\\malgun.ttf'
#path7 = 'C:\\Windows\\Fonts\\malgunbd.ttf'
#path8 = 'C:\\Windows\\Fonts\\DaehanR.ttf'
path9 = '/usr/share/fonts/truetype/nanum/NanumGothic.ttf'
fontprop1 = fm.FontProperties(fname=path1, size=12)
fontprop2 = fm.FontProperties(fname=path9, size=14)
fontprop3 = fm.FontProperties(fname=path9, size=13)
fontprop4 = fm.FontProperties(fname=path9, size=12)


def no_func(t):
    print ('버전: ', mpl.__version__)
    print ('설치 위치: ', mpl.__file__)
    print ('설정 위치: ', mpl.get_configdir())
    print ('캐시 위치: ', mpl.get_cachedir())
    print ('설정파일 위치: ', mpl.matplotlib_fname())
    font_list = fm.findSystemFonts(fontpaths=None, fontext='ttf')
    print(len(font_list))
    print(font_list[:10] )
    #print([(f.name, f.fname) for f in fm.fontManager.ttflist if 'Nanum' in f.name])
    print([(f.name, f.fname) for f in fm.fontManager.ttflist if 'Yj' in f.name])


def plot_text(xlist, ylist, xdelta, ydelta, w):
    name = w.name[1:]
    print(name)
    if len(name) >3 and name[-3] == '_':
        if name[-2] == 'U':
            valign = 'bottom'
        elif name[-2] == 'L':
            valign = 'top';
            ydelta = ydelta * -1
        elif name[-2] == 'C':
            valign = 'center'
            ydelta = 0
        else:
            valign = 'cneter'
            ydelta = 0
        
        if name[-1] == '1':
            halign = 'right'
            xdelta = xdelta * -1.2
        elif name[-1] == '2':
            halign = 'center'
            xdelta = 0
        elif name[-1] == '3':
            halign = 'left'
        else:
            halign = 'left'
        
        name = name[:-3]
    else:
        valign = 'bottom'
        ydelta = ydelta * -1
        halign = 'left'
        
    plt.text(xlist[w.pos]+xdelta, ylist[w.pos]+ydelta, name,fontproperties=fontprop4, color='k', ha=halign,va=valign)


def do_plot( xlist, ylist, w_list, s_list, name):
    font0 = FontProperties()
    alignment1 = {'horizontalalignment': 'center', 'verticalalignment': 'baseline'}
    alignment2 = {'horizontalalignment': 'left', 'verticalalignment': 'top'}
    alignment3 = {'horizontalalignment': 'right', 'verticalalignment': 'top'}
    # Show family options
    families = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
    font1 = font0.copy()
    font1.set_size('large')

    length = max(xlist)
    height = max(ylist)
    height = int((height+99.9)/100) *100
    height = max(height, 200)
    if (height == 400):
        dy1 = height *.827
        dy2 = height *.733
    elif height == 300:
        dy1 = height *.815
        dy2 = height *.736
    elif height == 600:
        dy1 = height *.821
        dy2 = height *.733
    else:
        dy1 = height *.82
        dy2 = height *.73
        
    xdelta = length / 200
    ydelta = height / 20

    figure = plt.figure(num=None, figsize=(15, 2.9), dpi=240, facecolor='w', edgecolor='k')
    #plt.axis([0, length, 0, height])
    ax = figure.gca()
    ax.set_xticks(numpy.arange(0, length, 20))
    if height >= 550:
        ax.set_yticks(numpy.arange(0, height, 100))
    else:
        ax.set_yticks(numpy.arange(0, height, 50))
        
    plt.grid(True, linewidth=0.8)

    #plt.plot([0,20,40,60,65,80,90,110,200,150], linewidth=1.5)
    for w in w_list:
        plt.plot((xlist[w.pos],xlist[w.pos]), (0,height), linewidth=2, color='red')
        km = '{:.0f} / {:.0f} km'.format(w.dkm, w.km)
        ascen = '{:.0f} / {:.0f} m'.format(w.dascen, w.ascen)
        # flip right & down if position is near 12/100
        if (xlist[w.pos] < length *.12) or (w.name[-2] == '_' and w.name[-1] == 'R'):
            if w.name[-2] == '_':
                w.name = w.name[:-2]
            plt.text(xlist[w.pos]+xdelta, height-100, w.name,fontproperties=fontprop3, color='darkblue', **alignment2)
            plt.text(xlist[w.pos]+xdelta, dy1-100, km,fontproperties=fontprop4, color='r', **alignment2)
            plt.text(xlist[w.pos]+xdelta, dy2-100, ascen,fontproperties=fontprop4, color='r', **alignment2)
        else:
            plt.text(xlist[w.pos]-xdelta, height-10, w.name,fontproperties=fontprop3, color='darkblue', **alignment3)
            plt.text(xlist[w.pos]-xdelta, dy1, km,fontproperties=fontprop4, color='r', **alignment3)
            plt.text(xlist[w.pos]-xdelta, dy2, ascen,fontproperties=fontprop4, color='r', **alignment3)
    
    for w in s_list:
        #plt.text(xlist[w.pos]+xdelta/2, ylist[w.pos]-24, w.name[1:],fontproperties=fontprop2, color='k', fontsize=11, **alignment2)
        plt.plot(xlist[w.pos], ylist[w.pos], '^', linewidth=3, color='r')
        plot_text(xlist, ylist, xdelta, ydelta, w)
    
    #plt.plot((345,345), (0,height), linewidth=3, color='red')
    #plt.plot((1,3), (1,6), color='g')
    plt.plot( xlist, ylist, color='b')

    #plt.ylabel('Altitude')
    #plt.xlabel('거리[km]', fontproperties=fontprop1)
    #plt.xlabel('my data', fontsize=14, color='red')
    #plt.title('Histogram of IQ')


    #plt.text(length*.5, height*.5, r'$\mu=100,\ \sigma=15$',color='red', fontsize=14)
    #plt.text(length*.2, height*.15, r'60 / 123 km \newline  320 / 763 m',color='green', fontsize=14)
    plt.text(6, height-12, name[0:-4], fontproperties=fontprop2, **alignment2, color='darkgreen')
    
    
    # pngname = name[0:-4] + '.png'
    #print(pngname)

    buf = io.BytesIO()
    plt.savefig(buf, dpi=200, bbox_inches='tight', format='png')
    plt.close()

    buf.seek(0)
    data = buf.read()

    buf.close()
    #plt.show()

    return data
    #no_func(1)


def main():
    x = numpy.linspace(-15,15,500)
    y = numpy.sin(x)/x
    x = x * 20 + 200
    y = y * 100 +200
    do_plot(x, y,'none')


if __name__ == '__main__':
	main()
