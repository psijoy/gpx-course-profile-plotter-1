import os
from flask import Flask, request, redirect, url_for, send_file
from werkzeug import secure_filename
from flask import send_from_directory
import gpxcpp
import io

UPLOAD_FOLDER = './files'
ALLOWED_EXTENSIONS = {'gpx'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename), code=307)
    return '''
    <!doctype html>
    <title>Upload gpx File</title>
    <h1>Upload gpx File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''


@app.route('/uploads/<filename>', methods=['GET', 'POST'])
def uploaded_file(filename):
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            image = gpxcpp.do_plot_gpx_to_png(file.stream._file.read().decode('UTF-8'), filename)

            return send_file(io.BytesIO(image), attachment_filename=filename, mimetype='image/png')
    return ''


app.run()
